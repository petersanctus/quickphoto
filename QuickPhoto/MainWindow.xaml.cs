﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.WindowsAPICodePack.Dialogs;
using QuickPhoto.Tools;

namespace QuickPhoto
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ImgHandler deckOfImagens;

        public MainWindow()
        {
            InitializeComponent();
            List<string> names = new List<string>();
            Thickness thickness = new Thickness(0, 2, 0, 0);
            string appDataLocal = Path.Combine(
                                        Environment.GetFolderPath(
                                        Environment.SpecialFolder.LocalApplicationData)
                                    , "QuickPhoto");
            if (!Directory.Exists(appDataLocal))
                Directory.CreateDirectory(appDataLocal);
            if (!File.Exists(Path.Combine(appDataLocal, "Class.ini")))
                File.Create(Path.Combine(appDataLocal, "Class.ini"));
            else
                names = Generic.GetClassNamesFromIniFile(appDataLocal).Split(',').ToList();
            ObservableCollection<CheckBox> listOfCheckBoxes_Names_OC = 
                    new ObservableCollection<CheckBox>(
                         names
                        .Select(name => new CheckBox() { Content = name, Margin = thickness }));
            ChildrenListBox.ItemsSource = listOfCheckBoxes_Names_OC;
            deckOfImagens = new ImgHandler(
                      textBoxSaveImagesDirectory
                    , listOfCheckBoxes_Names_OC
                    , imgDynamic
                    , selectedPhotoTextBox);
        }

        #region Load and Save actions
        private void BtnLoadDirectory_Click(object sender, RoutedEventArgs e)
        {
            using (var folderDialog = new CommonOpenFileDialog())
            {
                folderDialog.IsFolderPicker = true;
                folderDialog.Title = "Escolha a pasta do album de fotografias";
                if (Generic.ValidSelectedLocation(folderDialog))
                {
                    deckOfImagens.listOfImageAdresses = Directory
                            .GetFiles(folderDialog.FileName)
                            .Where(deckOfImagens.IsImage)
                            .ToArray<string>();
                    System.Windows.Forms.MessageBox.Show(
                            " " + deckOfImagens.listOfImageAdresses.Length + " imagens importadas."
                        , "Relatório de importação"
                        , System.Windows.Forms.MessageBoxButtons.OK
                        , System.Windows.Forms.MessageBoxIcon.Information);
                    if (deckOfImagens.listOfImageAdresses.Count() == 0)
                        return;
                    textBoxOrigem.Text = folderDialog.FileName;
                    try
                    {
                        deckOfImagens.OpenImage();
                    }
                    catch (NotSupportedException ex)
                    {
                        System.Windows.Forms.MessageBox.Show(
                                "Imagem não encontrada!\n\n"
                            , "Alerta"
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Warning);
                        deckOfImagens.OpenNextImage();
                    }
                }
            }
        }

        private void BtnSaveImagesMainDirectory_Click(object sender, RoutedEventArgs e)
        {
            using (var folderDialog = new CommonOpenFileDialog())
            {
                folderDialog.IsFolderPicker = true;
                folderDialog.Title = "Escolha a pasta de destino";
                if (Generic.ValidSelectedLocation(folderDialog))
                {
                    deckOfImagens.savePath = folderDialog.FileName;
                    textBoxSaveImagesDirectory.Text = folderDialog.FileName;
                    deckOfImagens.CheckImagesInDirectories();
                }
            }
        }

        #endregion

        private void BtnPrevious_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                deckOfImagens.SaveChildrensImages();
                deckOfImagens.OpenPrevImage();
            }
            catch (NullReferenceException listOfImageAdressesNull)
            {
                System.Windows.Forms.MessageBox.Show(
                        "Diretoria de imagens não seleccionada ou inválida!\n\n"
                    , "Alerta"
                    , System.Windows.Forms.MessageBoxButtons.OK
                    , System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                deckOfImagens.SaveChildrensImages();
                deckOfImagens.OpenNextImage();
            }
            catch (NullReferenceException listOfImageAdressesNull)
            {
                System.Windows.Forms.MessageBox.Show(
                        "Diretoria de imagens não seleccionada ou inválida!\n\n"
                    , "Alerta"
                    , System.Windows.Forms.MessageBoxButtons.OK
                    , System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }

        

        private void BtnActionToChildrenList_Click(object sender, RoutedEventArgs e)
        {
            bool hasNoneChecked = true;
            for (int index = deckOfImagens.listOfCheckBoxes_Names_OC.Count - 1; index >= 0; index--)
            {
                CheckBox targetCheckBox = deckOfImagens.listOfCheckBoxes_Names_OC[index];
                if ((bool)targetCheckBox.IsChecked)
                {
                    hasNoneChecked = false;
                    targetCheckBox.Content = textBoxImput.Text;
                }
            }
            if (hasNoneChecked)
                deckOfImagens.listOfCheckBoxes_Names_OC.Add(new CheckBox()
                {
                      Content = textBoxImput.Text
                    , Margin = new Thickness(0, 2, 0, 0)
                });
            textBoxImput.Text = "";
        }

        private void BtnDeleteFromChildrenList_Click(object sender, RoutedEventArgs e)
        {
            for (int index = deckOfImagens.listOfCheckBoxes_Names_OC.Count - 1; index >= 0; index--)
            {
                CheckBox targetCheckBox = deckOfImagens.listOfCheckBoxes_Names_OC[index];
                if ((bool)targetCheckBox.IsChecked)
                {
                    deckOfImagens.listOfCheckBoxes_Names_OC.Remove(targetCheckBox);
                }
            }
        }

        private void BtnSaveListOfChildren_Click(object sender, RoutedEventArgs e)
        {
            using (var fileDialog = new CommonSaveFileDialog())
            {
                fileDialog.Filters.Add(new CommonFileDialogFilter("Ini Files", "*.ini"));
                fileDialog.Title = "Escolha o ficheiro de classe para importar.";
                if (Generic.ValidSelectedLocation(fileDialog))
                {
                    var filePath = fileDialog.FileName;
                    if (!System.IO.Path.HasExtension(filePath))
                        filePath = filePath + ".ini";
                    Generic.WriteClassNamesToIniFile(filePath, deckOfImagens.listOfCheckBoxes_Names_OC);
                }
            }
        }

        private void BtnLoadListOfChildren_Click(object sender, RoutedEventArgs e)
        {
            List<string> names = new List<string>();
            Thickness thickness = new Thickness(0, 2, 0, 0);
            using (var fileDialog = new CommonOpenFileDialog())
            {
                fileDialog.Title = "Escolha o ficheiro de classe para importar.";
                if (Generic.ValidSelectedLocation(fileDialog))
                {
                    var filePath = fileDialog.FileName;
                    names = Generic.GetClassNamesFromIniFile(filePath).Split(',').ToList();
                    ObservableCollection<CheckBox> listOfCheckBoxes_Names_OC =
                        new ObservableCollection<CheckBox>(
                             names
                            .Select(name => new CheckBox() { Content = name, Margin = thickness }));
                    ChildrenListBox.ItemsSource = listOfCheckBoxes_Names_OC;
                    deckOfImagens = new ImgHandler(
                              textBoxSaveImagesDirectory
                            , listOfCheckBoxes_Names_OC
                            , imgDynamic
                            , selectedPhotoTextBox);
                }
            }
        }

        private void TextBoxImputOnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnActionToChildrenList_Click(sender, new RoutedEventArgs());
            }
            else if (e.Key == Key.Delete)
            {
                BtnDeleteImg_Click(sender, new RoutedEventArgs());
                btnDeleteImg.Focus();
            }
            else if (e.Key == Key.Escape)
            {
                imgDynamic.Focus();
            }
            else if (e.Key == Key.Right)
            {
                btnNext.Focus();
                deckOfImagens.OpenNextImage();
                e.Handled = true;
            }
            else if (e.Key == Key.Left)
            {
                btnPrevious.Focus();
                deckOfImagens.OpenPrevImage();
                e.Handled = true;
            }
        }

        private void BtnDeleteImg_Click(object sender, RoutedEventArgs e)
        {
            if (deckOfImagens.listOfImageAdresses == null || deckOfImagens.listOfImageAdresses?.Count() == 0)
                return;
            File.Delete(deckOfImagens.currentImagePath);
            deckOfImagens.listOfImageAdresses =
                    deckOfImagens.listOfImageAdresses
                    .Where(adress => adress != deckOfImagens.currentImagePath)
                    .ToArray();
            deckOfImagens.indexOfOpenedImage =
                    deckOfImagens.indexOfOpenedImage >= 0 ?
                    deckOfImagens.indexOfOpenedImage - 1 :
                    deckOfImagens.indexOfOpenedImage;
            deckOfImagens.OpenNextImage();
        }

        void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            string appDataLocal = Path.Combine(
                                        Environment.GetFolderPath(
                                        Environment.SpecialFolder.LocalApplicationData)
                                    , "QuickPhoto");
            Generic.WriteClassNamesToIniFile(appDataLocal, deckOfImagens.listOfCheckBoxes_Names_OC);
        }        
    }
}
