﻿using System;
using System.Collections.Generic;

namespace QuickPhoto.Tools
{
    public static class Enumerables
    {
        public static void ForEach<T>(this IEnumerable<T> @this, Action<T> action)
        {
            foreach (T item in @this)
            {
                action(item);
            }
        }

        public static string Join<T>(this IEnumerable<T> enumerable, string delimiter)
        {
            return string.Join(delimiter, enumerable);
        }
    }
}
