﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace QuickPhoto.Tools
{
    class ImgHandler
    {
        private ObservableCollection<CheckBox> _listOfCheckBoxes_Names_OC =
                new ObservableCollection<CheckBox>();
        private Image _imgDynamic;
        private TextBlock _selectedPhotoTextBox;
        private string[] _listOfImageAdresses;
        private string _savePath;
        private int _indexOfOpenedImage = 0;

        #region set get
        public string savePath 
        {
            get { return _savePath; }
            set { _savePath = value; }
        }
        public ObservableCollection<CheckBox> listOfCheckBoxes_Names_OC
        {
            get { return _listOfCheckBoxes_Names_OC; }
            set { _listOfCheckBoxes_Names_OC = value; }
        }
        public int indexOfOpenedImage
        {
            get { return _indexOfOpenedImage; }
            set { _indexOfOpenedImage = value; }
        }
        public string[] listOfImageAdresses
        {
            get { return _listOfImageAdresses; }
            set { _listOfImageAdresses = value; }
        }
        public Image imgDynamic
        {
            get { return _imgDynamic; }
            set { _imgDynamic = value; }
        }

        public string currentImageName
        {
            get { return listOfImageAdresses[indexOfOpenedImage].Split('\\').Last(); }
        }

        public string currentImagePath
        {
            get { return listOfImageAdresses[indexOfOpenedImage]; }
        }
        public TextBlock selectedPhotoTextBox
        {
            get { return _selectedPhotoTextBox; }
            set { _selectedPhotoTextBox = value; }
        }
        #endregion
        public ImgHandler(
                  TextBox textBoxSaveImagesDirectory
                , ObservableCollection<CheckBox> listOfCheckBoxes_Names_OC
                , Image imgDynamic
                , TextBlock selectedPhotoTextBox)
        {
            savePath = textBoxSaveImagesDirectory.Text;
            this.listOfCheckBoxes_Names_OC = listOfCheckBoxes_Names_OC;
            this.imgDynamic = imgDynamic;
            this.selectedPhotoTextBox = selectedPhotoTextBox;
        }

        public void SaveChildrensImages()
        {
            if (savePath != "")
            {
                Enumerables.ForEach(
                        listOfCheckBoxes_Names_OC
                        .Where(itemCheckBox => (bool)itemCheckBox.IsChecked)
                        .Select(itemCheckBox => itemCheckBox.Content.ToString())
                    , SaveImage);
                Enumerables.ForEach(
                        listOfCheckBoxes_Names_OC
                        .Where(itemCheckBox => !(bool)itemCheckBox.IsChecked)
                        .Select(itemCheckBox => itemCheckBox.Content.ToString())
                    , DeleteImage);
            }
        }

        public void OpenPrevImage()
        {
            try
            {
                indexOfOpenedImage =
                        indexOfOpenedImage > 0 ?
                        indexOfOpenedImage - 1 :
                        Convert.ToInt32(listOfImageAdresses?.Length - 1);
                OpenImage();
            }
            catch (FileNotFoundException ex)
            {
                System.Windows.Forms.MessageBox.Show(
                        "Imagem não encontrada!\n\n"
                    , "Alerta"
                    , System.Windows.Forms.MessageBoxButtons.OK
                    , System.Windows.Forms.MessageBoxIcon.Warning);
                OpenPrevImage();
            }
        }

        public void OpenNextImage()
        {
            try
            {
                indexOfOpenedImage =
                        indexOfOpenedImage < listOfImageAdresses?.Length - 1 ?
                        indexOfOpenedImage + 1 :
                        0;
                OpenImage();
            }
            catch (FileNotFoundException ex)
            {
                System.Windows.Forms.MessageBox.Show(
                        "Imagem não encontrada!\n\n"
                    , "Alerta"
                    , System.Windows.Forms.MessageBoxButtons.OK
                    , System.Windows.Forms.MessageBoxIcon.Warning);
                OpenNextImage();
            }
        }

        public void OpenImage()
        {
            if (listOfImageAdresses == null || listOfImageAdresses?.Count() == 0)
                return;
            try
            {
                BitmapImage imageToOpen = new BitmapImage();
                imageToOpen.BeginInit();
                imageToOpen.CacheOption = BitmapCacheOption.OnLoad;
                imageToOpen.UriSource = new Uri(currentImagePath);
                imageToOpen.EndInit();
                imgDynamic.Source = imageToOpen;
                selectedPhotoTextBox.Text =
                        indexOfOpenedImage
                        + 1
                        + " / "
                        + listOfImageAdresses.Count();
                CheckImagesInDirectories();
            }
            catch (NullReferenceException exceptionRef)
            {

            }
        }

        public void CheckImagesInDirectories()
        {
            foreach (var itemCheckBox in listOfCheckBoxes_Names_OC)
            {
                String childName = (string)itemCheckBox.Content;
                if (Directory.Exists(Path.Combine(savePath, childName)))
                {
                    if (File.Exists(Path.Combine(savePath, childName, currentImageName)))
                        itemCheckBox.IsChecked = true;
                    else
                        itemCheckBox.IsChecked = false;
                }
                else
                    itemCheckBox.IsChecked = false;
            }
        }

        public void SaveImage(string childName)
        {
            if (Directory.Exists(Path.Combine(savePath, childName)) == false)
                Directory.CreateDirectory(Path.Combine(savePath, childName));
            if (!File.Exists(Path.Combine(savePath, childName, currentImageName)))
                File.Copy(
                        currentImagePath
                    , Path.Combine(savePath, childName, currentImageName));
        }

        public void DeleteImage(string childName)
        {
            if (Directory.Exists(Path.Combine(savePath, childName)) == true)
                File.Delete(Path.Combine(savePath, childName, currentImageName));
        }

        public bool IsImage(string imageAdress)
        {
            try
            {
                BitmapImage imageToOpen = new BitmapImage();
                imageToOpen.BeginInit();
                imageToOpen.CacheOption = BitmapCacheOption.OnLoad;
                imageToOpen.UriSource = new Uri(imageAdress);
                imageToOpen.EndInit();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
