﻿using IniParser;
using IniParser.Model;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;

namespace QuickPhoto.Tools
{
    class Generic
    {
        public static String GetClassNamesFromIniFile(string inputString)
        {
            FileIniDataParser parser = new FileIniDataParser();
            if (!System.IO.Path.HasExtension(inputString)) //check if is not a file
            {               
                return parser.ReadFile(
                        Path.Combine(
                              inputString
                            , "Class.ini"))
                        ["Classe"]["Nomes"];
            }
            return parser.ReadFile(inputString)["Classe"]["Nomes"];
        }

        public static void WriteClassNamesToIniFile(string outputString, ObservableCollection<CheckBox> listOfCheckBoxes_Names_OC)
        {
            FileIniDataParser parser = new FileIniDataParser();
            IniData iniFile = new IniData();
            iniFile["Classe"]["Nomes"] = listOfCheckBoxes_Names_OC
                           .Select(checkBox => checkBox.Content.ToString())
                           .Join(",");
            if (!System.IO.Path.HasExtension(outputString)) //check if is not a file
            {
                parser.WriteFile(Path.Combine(outputString, "Class.ini"), iniFile);
                return;
            }
            parser.WriteFile(outputString, iniFile);
        }

        public static bool ValidSelectedLocation(CommonOpenFileDialog folderDialog)
        {
            return folderDialog.ShowDialog() == CommonFileDialogResult.Ok;
        }

        public static bool ValidSelectedLocation(CommonSaveFileDialog folderDialog)
        {
            return folderDialog.ShowDialog() == CommonFileDialogResult.Ok;
        }
    }
}
